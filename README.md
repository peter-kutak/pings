# pings

Ping utility for console with graphical output.


## Compilation
```
pip install pythonping
pip install nuitka

nuitka --follow-imports pings.py
su -c "/sbin/setcap cap_net_raw+ep pings.bin"
```

## Screenshots

![Alt text](/docs/pings-chart.png?raw=true "Chart")
![Alt text](/docs/pings-histogram.png?raw=true "Histogram")
![Alt text](/docs/pings-calendar.png?raw=true "Calendar")
