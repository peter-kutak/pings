"""Program zobrazujuci aktualny stav"""
import sys
import argparse
from time import sleep
import curses
from curses import wrapper
import time
import math
import textwrap
import pythonping
from pythonping import ping

MAX_WIDTH = 180
COLOR_HIST_BAR = 8
COLOR_HIST_SPACE = 9
COLOR_MENU_HL = 10
COLOR_MENU = 11
COLOR_MENU_CLOCK = 12
COLOR_CALENDAR = 13
COLOR_CALENDAR_ERROR = 14
COLOR_CALENDAR_MIX = 15
COLOR_CALENDAR_OK = 16
msg_debug = ''

class Serie:
    def __init__(self, maxsize=60):
        self._values = []
        self._maxsize = maxsize

    def add(self, value):
        self._values.append(value)
        self._trim()

    def getAll(self):
        return self._values

    def getSlice(self, length):
        return self._values[-length:]

    def getLast(self):
        if len(self._values) <= 0:
            return None
        return self._values[-1]

    def _trim(self):
        while len(self._values) > self._maxsize:
            self._values.pop(0)

class Histogram:
  def __init__(self, min, max, buckets):
    self._buckets = buckets
    self._min = min
    self._lmin = math.log(min)
    self._max = max
    self._lmax = math.log(max)
    self._histogram = [0] * buckets
    self._maxCount = 1

  @property
  def buckets(self):
    return self._buckets

  @property
  def maxCount(self):
    return self._maxCount

  def value2index(self, value):
    result = ((math.log(value) - self._lmin) / (self._lmax - self._lmin)) * self.buckets
    return int(result)

  def put(self, value):
    if value > self._max:
      value = self._max
    if value < self._min:
      value = self._min
    i = self.value2index(value)
    c = self._histogram[i] + 1
    self._histogram[i] = c
    if c > self._maxCount: self._maxCount = c
  
  def get(self, index):
    return self._histogram[index]

class Calendar:
  def __init__(self):
    self._reinit()

  def _reinit(self):
    self._calendar = [{'r':0, 'e':0}] * 24
    self._today = time.time() // (24*60*60)

  def _verify_today(self):
    '''Calendar keep data only for today'''
    if self._today != (time.time() // (24*60*60)):
      self._reinit()

  def put(self, value):
    self._verify_today()
    t = time.time()
    h = int(t//3600%24)
    if value is not None:
      self._calendar[h]['r'] = self._calendar[h]['r'] + 1
    else:
      self._calendar[h]['e'] = self._calendar[h]['e'] + 1
  
  def get(self, hour):
    self._verify_today()
    return self._calendar[hour]

def YlogScale(value):
  value = math.log(value)/math.log(2000)
  return Y(value, 0, 1)

def Y(value, minValue = 40, maxValue = 60, topY = 0, bottomY = 20):
  """Prepocitava hodnotu na polohu na obrazovke """
  res = int(((maxValue - value) / (maxValue-minValue)) * (bottomY-topY)) + topY
  if res<topY: res = topY
  if res>bottomY: res = bottomY
  return res

def dimmerCharH(value):
  rv = int(value/100*18)
  v = rv - 9
  if v > 9: v = 9
  if v < 0: v = 0
  if v == 0: return ' '
  return chr(int(9600+v))

def dimmerCharL(value):
  rv = int(value/100*18)
  v = rv
  if v > 9: v = 9
  if v < 0: v = 0
  if v == 0:
    if value >0:
      return chr(int(9600+1))
    else:
      return '.'
  return chr(int(9600+v))

def dimmerChar(value):
  if value > 80:
    return  '\u2588'
  if value > 60:
    return '\u2593'
  if value > 40:
    return '\u2592'
  if value >= 10:
    return '\u2591'
  return '_'

def dispChart(win, rows, cols, data):
  chart_width = cols-20
  if chart_width <= 0: chart_width = 1
  value_col = 5 + chart_width + 3
  chart_height = 23
  #skeleton
  x = 0
  win.addstr(YlogScale(2000), x, '2k -')
  win.addstr(YlogScale(100), x, '100-')
  win.addstr(YlogScale(10), x, '10 -')
  win.addstr(YlogScale(1), x, '1 -')
  x = 5
  for i in range(0, chart_width):
    x = x +1
    win.addstr(YlogScale(2000), x, '-')
    win.addstr(YlogScale(100), x, '-')
    win.addstr(YlogScale(10), x, '-')
    win.addstr(YlogScale(1), x, '-')
  for i in range(0, chart_height):
    win.addstr(i, 5, '|')
    win.addstr(i, 5+chart_width+1, '|')
  #delay
  x = 5
  win.addstr(0, value_col, "elapsed time")
  delay = data['delay'].getLast()
  win.addstr(1, value_col, "~~~~.~~ ms" if delay is None else "{:7.2f} ms".format(delay))
  for v in data['delay'].getSlice(chart_width):
    x = x + 1
    if v is None:
      win.addstr(0, x, 'X', curses.color_pair(3))
    else:
      y = YlogScale(v)
      win.addstr(y, x, '*', curses.color_pair(1))

  #x = 5
  #for v in vytaz.getSlice(chart_width):
  #  x = x + 1
  #  win.addstr(21, x, dimmerCharH(v[2]))
  #  win.addstr(22, x, dimmerCharL(v[2]))
 
def dispHistogram(win, rows, cols, data):
  #skeleton
  x = 0
  win.addstr(YlogScale(2000), x, '2k -')
  win.addstr(YlogScale(100), x, '100-')
  win.addstr(YlogScale(10), x, '10 -')
  win.addstr(YlogScale(1), x, '1 -')
  buckets = data['histogram'].buckets
  for i in range(buckets):
    width = cols - 15
    if width <= 0: width = 1
    v = data['histogram'].get(i)
    vn = v / data['histogram'].maxCount
    if vn > 1: vn = 1
    vn = int(vn * width)
    text = "{}                                                                                                                                                             ".format(v)
    win.addstr(buckets-i, 5, text[:vn], curses.color_pair(COLOR_HIST_BAR))
    win.addstr(buckets-i, 5+vn, text[vn:width], curses.color_pair(COLOR_HIST_SPACE))
  
def dispErrors(win, rows, cols, data):
  win.addstr(0,0,"Error {}".format(data['error']))
  win.addstr(1,0,"PermissionError {}".format(data['PermissionError']))

def dispCalendar(win, rows, cols, data):
  for h in range(24):
    r = data['calendar'].get(h)['r']
    e = data['calendar'].get(h)['e']
    color = curses.color_pair(COLOR_CALENDAR)
    if r > 0:
      if e > 0:
        color = curses.color_pair(COLOR_CALENDAR_MIX)
      else:
        color = curses.color_pair(COLOR_CALENDAR_OK)
    else:
      if e > 0:
        color = curses.color_pair(COLOR_CALENDAR_ERROR)
    win.addstr(h//6 * 5 + 3, (h%6)*12, ' R:{:6d} '.format(r), color)
    win.addstr(h//6 * 5 + 4, (h%6)*12, ' E:{:6d} '.format(e), color)

def tick(win, rows, cols, mode, data, key):
  """Vykresli data na obrazovku """
  if mode == 0:
    dispChart(win, rows, cols, data)
  if mode == 1:
    dispHistogram(win, rows, cols, data)
  if mode == 2:
    dispCalendar(win, rows, cols, data)
  if mode == 3:
    dispErrors(win, rows, cols, data)
  #real time
  t = time.time()
  if cols >= 10:
    win.addstr(24, cols-10, ' {:02d}:{:02d}:{:02d} '.format(int(t//3600%24),int((t//60)%60), int(t%60)), curses.color_pair(COLOR_MENU_CLOCK))
  #console
  win.addstr(23, 0, textwrap.shorten(data['console'], cols))
  #menu
  win.addstr(24, 0, 'c ', curses.color_pair(COLOR_MENU_HL))
  win.addstr(24, 0+2, 'chart   ', curses.color_pair(COLOR_MENU))
  win.addstr(24, 10, 'h ', curses.color_pair(COLOR_MENU_HL))
  win.addstr(24, 10+2, 'hist.   ', curses.color_pair(COLOR_MENU))
  win.addstr(24, 20, 'k ', curses.color_pair(COLOR_MENU_HL))
  win.addstr(24, 20+2, 'calendar', curses.color_pair(COLOR_MENU))
  win.addstr(24, 30, 'e ', curses.color_pair(COLOR_MENU_HL))
  win.addstr(24, 30+2, 'errors  ', curses.color_pair(COLOR_MENU))
  win.addstr(24, 40, 'q ', curses.color_pair(COLOR_MENU_HL))
  win.addstr(24, 40+2, 'quit    ', curses.color_pair(COLOR_MENU))
  #debug
  #win.addstr(23, 0, '{} {}'.format(key.encode('utf8'),key=="\x12"))
  #global msg_debug
  #win.addstr(24, 0, msg_debug)

def enterTarget(parent):
  #curses.nocbreak()
  #curses.echo()
  #curses.curs_set(True)
  result = parent.getstr(0,0,20)
  return result.decode(sys.stdin.encoding)

def main(stdscr):
  """Hlavna funkia programu """
  global args
  global msg_debug
  curses.cbreak()
  curses.curs_set(False)
  curses.start_color()
  curses.init_pair(1, curses.COLOR_GREEN, curses.COLOR_BLACK)
  curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
  curses.init_pair(3, curses.COLOR_YELLOW, curses.COLOR_BLACK)
  curses.init_pair(COLOR_HIST_BAR, curses.COLOR_BLACK, curses.COLOR_WHITE)
  curses.init_pair(COLOR_HIST_SPACE, curses.COLOR_WHITE, curses.COLOR_BLACK)
  curses.init_pair(COLOR_MENU_HL, curses.COLOR_RED, curses.COLOR_WHITE)
  curses.init_pair(COLOR_MENU, curses.COLOR_WHITE, curses.COLOR_BLUE)
  curses.init_pair(COLOR_MENU_CLOCK, curses.COLOR_BLACK, curses.COLOR_CYAN)
  curses.init_pair(COLOR_CALENDAR, curses.COLOR_BLACK, curses.COLOR_WHITE)
  curses.init_pair(COLOR_CALENDAR_ERROR, curses.COLOR_WHITE, curses.COLOR_RED)
  curses.init_pair(COLOR_CALENDAR_MIX, curses.COLOR_RED, curses.COLOR_YELLOW)
  curses.init_pair(COLOR_CALENDAR_OK, curses.COLOR_BLUE, curses.COLOR_GREEN)
  stdscr.nodelay(True)
  curses.halfdelay(10)
  # This is the necessary initial refresh
  stdscr.refresh()
  max_rows = 50
  max_cols = 200
  pad = curses.newpad(max_rows, max_cols)
  runApp = True
  key = ''
  redraw = False
  dispMode = 0
  histogram_buckets = 20
  data = {'PermissionError': 0, 'error':0, 'delay': Serie(maxsize=MAX_WIDTH), 'console':'', 'histogram': Histogram(1,2000,histogram_buckets), 'calendar': Calendar()}
  while runApp:
    if redraw:
      pad.clear()
      redraw = False
    else:
      pad.erase()
    rows, cols = stdscr.getmaxyx()
    prows = min(rows, max_rows)
    pcols = min(cols, max_cols)
    tick(pad, prows, pcols, dispMode, data, key)
    pad.refresh(0,0, 0,0, rows-1,cols-1)
    try:
      key = pad.getkey()
    except Exception:
      #no input
      key = ''
    if key == '\x12':
      #CtrlR
      redraw = True
    if key == 'q':
      runApp = False
    if key == 't':
      msg_debug = enterTarget(stdscr)
    if key == 'c':
      dispMode = 0
    if key == 'h':
      dispMode = 1
    if key == 'k':
      dispMode = 2
    if key == 'e':
      dispMode = 3
    sleep(0.1)
    try:
      #ping('127.0.0.1')
      target = '127.0.0.1'
      target = '192.168.1.206'
      target = '192.168.1.20'
      target = args.destination
      timeout = 2
      count = 1
      size = args.s
      options = ()
      out = sys.stderr
      verbose = False
      payload = pythonping.utils.random_text(size)
      provider = pythonping.payload_provider.Repeat(payload, count)
      comm = pythonping.executor.Communicator(target, provider, timeout, socket_options=options, verbose=verbose, output=out)
      comm.run()
      msg = ""
      for r in comm.responses:
        data['console']="{}".format(r)
        if r.success:
          time_ms = r.time_elapsed * 1000
          data['delay'].add(time_ms)
          data['histogram'].put(time_ms)
          data['calendar'].put(time_ms)
        else:
          msg_debug = r.error_message
          data['delay'].add(None)
          data['calendar'].put(None)
          data['error'] = data['error'] + 1
          data[r.error_message] = data.get(r.error_message, 0) + 1
    except PermissionError as e:
      data['PermissionError'] = data['PermissionError'] + 1
      data['console']="{}".format(e)



parser = argparse.ArgumentParser()
parser.add_argument('destination')
#parser.add_argument('-c', metavar='count')
parser.add_argument('-s', metavar='packetsize', type=int, default=56)
args = parser.parse_args()
print(args, file=sys.stderr)
wrapper(main)

